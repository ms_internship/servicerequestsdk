package mobilestudio.io.sdk.requestsdk;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mobilestudio.io.sdk.requestsdk.provider.Provider;
import mobilestudio.io.sdk.requestsdk.provider.ProviderManager;
import mobilestudio.io.sdk.requestsdk.request.Request;

/**
 * Created by pisoo on 10/5/2017.
 */

public class ServiceManager implements Serializable {
    List<Provider> providers;
    ProviderManager manager;

    public ServiceManager(ProviderManager providerManager) {
        providers = new ArrayList<>();
        manager = providerManager;
    }

    public void addProfile(Provider profile) {
        providers.add(profile);
    }

    public List<Provider> matchRequest(Request request) {
        providers = manager.getApproved();

        Log.i("eeer",providers.size()+"");
        List<Provider> matchedRequest = new ArrayList<>();

        for (Provider provider : providers) {

            if (provider.match(request)){
                matchedRequest.add(provider);
                Log.i("isArovApp",providers.size()+"");
            }

        }
        return matchedRequest;
    }

}
