package mobilestudio.io.sdk.requestsdk.provider;

import java.io.Serializable;

/**
 * Created by pisoo on 10/8/2017.
 */

public class UberValidator implements ProviderValidator,Serializable {

    @Override
    public void validate(Provider provider, ValidatorCallback validatorCallback) {

        boolean ifValidName = false;
        boolean ifValidAddress = false;
        boolean ifValidPhone = false;
        boolean ifValidKindOfCar = false;

        if (provider.profileCriteria.getAddress().length() > 4) {
            ifValidAddress = true;
        }
        ////
        if (provider.profileCriteria.getName().length() > 4) {
            ifValidName = true;
        }

        if (provider.profileCriteria.getPhone().length() > 10) {
            ifValidPhone = true;
        }

        if (provider.profileCriteria.getKindOfCar().length() > 2) {
            ifValidKindOfCar = true;
        }

        if (ifValidName && ifValidKindOfCar && ifValidAddress && ifValidPhone) {
            validatorCallback.onValidationSuccess(provider);
        } else {
            validatorCallback.onValidationFailed();
        }
    }


}
