package mobilestudio.io.sdk.requestsdk.service;

/**
 * Created by Sayed on 10/5/2017.
 */

public abstract class CriteriaMatcher<T> {
    T val;

    public T getVal() {
        return val;
    }

    public void setVal(T val) {
        this.val = val;
    }

    public abstract boolean match(T value);
}
