package mobilestudio.io.sdk.requestsdk.provider;

/**
 * Created by mah_y on 08/10/2017.
 */

public interface ValidatorCallback {
    void onValidationSuccess(Provider provider);
    void onValidationFailed();
}
