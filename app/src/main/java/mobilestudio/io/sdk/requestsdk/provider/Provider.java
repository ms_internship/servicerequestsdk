package mobilestudio.io.sdk.requestsdk.provider;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import mobilestudio.io.sdk.requestsdk.request.Request;
import mobilestudio.io.sdk.requestsdk.service.Service;

/**
 * Created by pisoo on 10/8/2017.
 */

public abstract class Provider implements ValidatorCallback, Serializable {
    ProfileCriteria profileCriteria;
    ProviderValidator providerValidator;
    List<Service> serviceList;

    public Provider(){
        serviceList = new ArrayList<>();
    }

    public void validateProvider(Provider provider, ValidatorCallback callback) {
        providerValidator = new UberValidator();
        providerValidator.validate(provider, callback);
    }

    public boolean match(Request request) {
        boolean flag = true;
        for (Service service : serviceList) {
            flag &= service.match(request);
        }
        return flag;
    }

    public void addService(Service service) {
        serviceList.add(service);
    }

}
