package mobilestudio.io.sdk.requestsdk.provider;

/**
 * Created by pisoo on 10/8/2017.
 */

interface ProviderValidator {
    void validate(Provider provider ,ValidatorCallback validatorCallback);
}
