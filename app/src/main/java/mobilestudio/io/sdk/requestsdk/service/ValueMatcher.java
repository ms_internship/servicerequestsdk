package mobilestudio.io.sdk.requestsdk.service;


/**
 * Created by Sayed on 10/5/2017.
 */

public class ValueMatcher extends CriteriaMatcher<String> {
    String serviceWord;

    public ValueMatcher(String word) {
        this.serviceWord = word;
    }

    public boolean match(String serviceWord) {
        return serviceWord.equals(this.serviceWord);
    }
}
