package mobilestudio.io.sdk.requestsdk.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pisoo on 10/5/2017.
 */

public abstract class Request  {
    Map<String , Criteria> criterias ;

   public  Map<String , Criteria>  getCriterias (){
        return criterias ;
    }

    public Request() {
        criterias = new HashMap<>();
    }
}
