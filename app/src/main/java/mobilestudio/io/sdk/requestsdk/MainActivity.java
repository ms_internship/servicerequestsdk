package mobilestudio.io.sdk.requestsdk;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.constraint.Guideline;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobilestudio.io.sdk.requestsdk.activities.RequestActivity;
import mobilestudio.io.sdk.requestsdk.provider.ProfileCriteria;
import mobilestudio.io.sdk.requestsdk.provider.Provider;
import mobilestudio.io.sdk.requestsdk.provider.ProviderManager;
import mobilestudio.io.sdk.requestsdk.provider.RegistrationCallback;
import mobilestudio.io.sdk.requestsdk.provider.UberProvider;
import mobilestudio.io.sdk.requestsdk.provider.ValidatorCallback;
import mobilestudio.io.sdk.requestsdk.request.GpsCriteria;
import mobilestudio.io.sdk.requestsdk.request.KeyWordCriteria;
import mobilestudio.io.sdk.requestsdk.request.UberRequest;
import mobilestudio.io.sdk.requestsdk.service.CriteriaMatcher;
import mobilestudio.io.sdk.requestsdk.service.GpsMatcher;
import mobilestudio.io.sdk.requestsdk.service.Service;

public class MainActivity extends AppCompatActivity implements RegistrationCallback {

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.phone)
    EditText phone;

    @BindView(R.id.addres)
    EditText address;

    @BindView(R.id.car_type)
    EditText carType;

    @BindView(R.id.guideline)
    Guideline guideline;


    ProviderManager manager;
    ServiceManager serviceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        TextView textView = (TextView) findViewById(R.id.helloWorld);

        // first
        Location firstLocation = new Location("mylocation");
        firstLocation.setLatitude(30.0);
        firstLocation.setLongitude(30.0);
        //sec
        Location secondLocation = new Location("mylocation");
        secondLocation.setLatitude(30.0);
        secondLocation.setLongitude(30.0);
        //third
        Location thirdLocation = new Location("mylocation");
        thirdLocation.setLatitude(50.0);
        thirdLocation.setLongitude(30.0);

        //---
        manager = new ProviderManager();
        serviceManager = new ServiceManager(manager);

        Service service1 = new Service();

        service1.addCriteria("Gps", new GpsCriteria(firstLocation));
        service1.addCriteria("Keyword", new KeyWordCriteria("uber"));

        ProfileCriteria profileCriteria = new ProfileCriteria();
        profileCriteria.setAddress("Maadi");
        profileCriteria.setName("Sayed");
        profileCriteria.setPhone("01111332137");
        profileCriteria.setKindOfCar("X");

        UberProvider uberProvider1 = new UberProvider(profileCriteria);

        service1.addMatcher(new GpsMatcher(firstLocation));
        uberProvider1.addService(service1);
        manager.Register(uberProvider1, this);
//----------------

        UberProvider uberProvider2 = new UberProvider(profileCriteria);
        Service service2 = new Service();

        service2.addCriteria("Gps", new GpsCriteria(secondLocation));
        service2.addCriteria("Keyword", new KeyWordCriteria("uber"));


        GpsMatcher gpsMatcher1 = new GpsMatcher(secondLocation);
        service2.addMatcher(gpsMatcher1);
        uberProvider2.addService(service2);
        manager.Register(uberProvider2, this);

//---------------
        UberProvider uberProvider3 = new UberProvider(profileCriteria);
        Service service3 = new Service();

        GpsMatcher gpsMatcher = new GpsMatcher(thirdLocation);

        service3.addCriteria("Gps", new GpsCriteria(thirdLocation));
        service3.addCriteria("Keyword", new KeyWordCriteria("uber"));
        service3.addMatcher(gpsMatcher);

        uberProvider3.addService(service3);
        manager.Register(uberProvider3, this);

        Constants.serviceManager = serviceManager;

    }

    @OnClick(R.id.registerButton)
    void click() {

        ProfileCriteria profileCriteria = new ProfileCriteria();


        profileCriteria.setAddress(address.getText().toString());
        profileCriteria.setName(name.getText().toString());
        profileCriteria.setPhone(phone.getText().toString());
        profileCriteria.setKindOfCar(carType.getText().toString());

        UberProvider uberProvider = new UberProvider(profileCriteria);

        uberProvider.validateProvider(uberProvider, new ValidatorCallback() {
            @Override
            public void onValidationSuccess(Provider provider) {
                manager.Register(provider, MainActivity.this);
            }

            @Override
            public void onValidationFailed() {
                Toast.makeText(getApplicationContext(), "Validation Failed ya 3m armt", Toast.LENGTH_LONG).show();

            }
        });
    }

    @OnClick(R.id.go_to_request_btn)
    void makeRequest() {
        Intent intent = new Intent(MainActivity.this, RequestActivity.class);
        intent.putExtra("RequestActivity", serviceManager);
        startActivity(intent);
    }

    @Override
    public void onFailed(String failureMessage) {
        Toast.makeText(getApplicationContext(), failureMessage, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onPending() {
        //     Toast.makeText(getApplicationContext(), "pending", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onApproved() {
        //   Toast.makeText(getApplicationContext(), "Approved", Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.showButton)
    void show() {
        // Toast.makeText(getApplicationContext(), "Approved : " + manager.getApproved().size() + "Pending : " + manager.getPending().size(), Toast.LENGTH_LONG).show();


    }
}
