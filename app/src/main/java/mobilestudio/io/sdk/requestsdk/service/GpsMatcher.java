package mobilestudio.io.sdk.requestsdk.service;

import android.location.Location;
import android.util.Log;

import java.util.List;

/**
 * Created by Sayed on 10/5/2017.
 */

public class GpsMatcher extends CriteriaMatcher<Location> {
    Location serviceLocation;

    public GpsMatcher(Location location) {
        this.serviceLocation = location;
    }

    @Override
    public boolean match(Location location) {
  return (Math.abs(location.getLatitude() - this.serviceLocation.getLatitude()) ==0)
                && (Math.abs(location.getAltitude() - this.serviceLocation.getAltitude())== 0);
    }

}
