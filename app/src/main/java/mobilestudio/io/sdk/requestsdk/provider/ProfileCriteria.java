package mobilestudio.io.sdk.requestsdk.provider;

import java.io.Serializable;

/**
 * Created by pisoo on 10/8/2017.
 */

public class ProfileCriteria implements Serializable {
    String name;
    String phone;
    String address;
    String kindOfCar;

    public ProfileCriteria() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setKindOfCar(String kindOfCar) {
        this.kindOfCar = kindOfCar;
    }



    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getKindOfCar() {
        return kindOfCar;
    }

}
