package mobilestudio.io.sdk.requestsdk.service;

import android.location.Location;
import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mobilestudio.io.sdk.requestsdk.request.Criteria;
import mobilestudio.io.sdk.requestsdk.request.Request;

/**
 * Created by Sayed on 10/5/2017.
 */

public abstract class ServiceProfile {
    private Map<String, Criteria> criterias = new HashMap<>();
    boolean test = false;

    List<CriteriaMatcher> criteriaMatchers;

    public void addCriteria(String str , Criteria criteria) {
        this.criterias.put(str, criteria);
    }

    public boolean match(Request request) {
        test = true;
        criterias = request.getCriterias();
        Location lacation = (Location) criterias.get("Gps").getValue();
       // String word = (String) criterias.get("Keyword").getValue();
        Log.i("dtrd",criteriaMatchers.size()+"");

        for (CriteriaMatcher c : criteriaMatchers) {
            if (c instanceof GpsMatcher)
               test &= ((GpsMatcher) c).match(lacation);
        }
    return test;
    }
}
/*  if (c instanceof ValueMatcher)
               test &= ((ValueMatcher) c).match(word);*/