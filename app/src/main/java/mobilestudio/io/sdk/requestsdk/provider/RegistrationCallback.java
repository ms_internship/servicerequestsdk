package mobilestudio.io.sdk.requestsdk.provider;

/**
 * Created by mah_y on 08/10/2017.
 */

public interface RegistrationCallback {
    void onFailed(String failureMessage);
    void onPending();
   // void onRefused();
    void onApproved();
}
