package mobilestudio.io.sdk.requestsdk.request;

import android.location.Location;

/**
 * Created by pisoo on 10/5/2017.
 */

public class GpsCriteria extends Criteria<Location> {

    public GpsCriteria(Location value) {
        super(value);
    }

    @Override
    public Location getValue() {
        return value;
    }

    @Override
    public void setValue(Location value) {

    }
}
