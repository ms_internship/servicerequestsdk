package mobilestudio.io.sdk.requestsdk.request;

/**
 * Created by pisoo on 10/5/2017.
 */

public abstract class Criteria <T>  {
    T value ;


    public Criteria(T value) {
        this.value = value;
    }

    public abstract T getValue();

    public abstract void setValue(T value);


}
