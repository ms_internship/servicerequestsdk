package mobilestudio.io.sdk.requestsdk.service;


import java.io.Serializable;
import java.util.ArrayList;


public  class Service extends ServiceProfile implements Serializable{

    public Service() {
        criteriaMatchers = new ArrayList<>();
    }

    public void addMatcher(CriteriaMatcher criteriaMatcher) {
        criteriaMatchers.add(criteriaMatcher);
    }

}
