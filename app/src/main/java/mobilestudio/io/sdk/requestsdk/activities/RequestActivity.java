package mobilestudio.io.sdk.requestsdk.activities;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobilestudio.io.sdk.requestsdk.Constants;
import mobilestudio.io.sdk.requestsdk.R;
import mobilestudio.io.sdk.requestsdk.ServiceManager;
import mobilestudio.io.sdk.requestsdk.provider.Provider;
import mobilestudio.io.sdk.requestsdk.provider.ProviderManager;
import mobilestudio.io.sdk.requestsdk.request.UberRequest;

public class RequestActivity extends AppCompatActivity {
    @BindView(R.id.longitude)
    EditText etLongitude;
    @BindView(R.id.latitude)
    EditText etLatitude;
    @BindView(R.id.keyWord)
    EditText etKeyword;
    String longitude, latitude, keyword;

    ProviderManager manager;
    ServiceManager serviceManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);
        ButterKnife.bind(this);
        //serviceManager = (ServiceManager) getIntent().getExtras().getSerializable("RequestActivity");

       // manager=new ProviderManager();
       serviceManager= Constants.serviceManager;
    }

    @OnClick(R.id.request)
    void request() {

        longitude = etLongitude.getText().toString();
        latitude = etLatitude.getText().toString();
        keyword = etKeyword.getText().toString();
        Location location = new Location("mylocation");
        location.setLatitude(Double.parseDouble(latitude));
        location.setLongitude(Double.parseDouble(longitude));
        UberRequest uberRequest = new UberRequest(location, "uber");
        List<Provider> matchedProviders=serviceManager.matchRequest(uberRequest);

        Toast.makeText(this,matchedProviders.size()+"",Toast.LENGTH_LONG).show();

    }
}
