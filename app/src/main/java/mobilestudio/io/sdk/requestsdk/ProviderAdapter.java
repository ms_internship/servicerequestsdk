package mobilestudio.io.sdk.requestsdk;

/**
 * Created by Domtyyyyyy on 10/12/2017.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobilestudio.io.sdk.requestsdk.provider.Provider;

/**
 * Created by Domtyyyyyy on 8/29/2017.
 */

public class ProviderAdapter extends RecyclerView.Adapter<ProviderAdapter.providerHolder> {

    List<Provider> providerList;
    Context context;

    public ProviderAdapter(Context context) {
        //  this.list = list;
        this.context = context;
        providerList = new ArrayList<>();
    }

    public void setList(List<Provider> providers) {
        this.providerList = providers;
    }

    @Override
    public providerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.povider_item, parent, false);
        providerHolder holder = new providerHolder(row);
        return holder;
    }

    @Override
    public void onBindViewHolder(providerHolder holder, int position) {
        Provider provider = providerList.get(position);
      //  holder.providerName.setText(provider.);

}

    @Override
    public int getItemCount() {
        return providerList.size();
    }

    class providerHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.providerName)
        TextView providerName;


        public providerHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
