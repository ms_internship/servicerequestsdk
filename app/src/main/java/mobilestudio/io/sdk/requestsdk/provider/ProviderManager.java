package mobilestudio.io.sdk.requestsdk.provider;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mah_y on 08/10/2017.
 */

public class ProviderManager implements Serializable {

    Configeration configeration;
    List<Provider> approved;
    List<Provider> pending;


    public ProviderManager() {
        configeration = new Configeration();
        approved = new ArrayList<>();
        pending = new ArrayList<>();
    }
    public List<Provider> getApproved(){
        return approved;
    }
    public List<Provider> getPending(){
        return pending;
    }

    public void Register(Provider provider, RegistrationCallback listener) {
        if (configeration.autoApproved) {
            approved.add(provider);
            listener.onApproved();
        } else if (configeration.multiRequest) {
            approved.add(provider);
            listener.onApproved();
        } else if (!configeration.autoApproved) {
            pending.add(provider);
            listener.onPending();
        } else {
            listener.onFailed("registration failed ya 3m arm6");
        }
//        else if (!configeration.autoApproved && !configeration.multiRequest && !configeration.multiService) {
////            listener.onRefused();
//        }

    }


}