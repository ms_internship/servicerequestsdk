package mobilestudio.io.sdk.requestsdk.request;

import android.location.Location;

/**
 * Created by pisoo on 10/5/2017.
 */

public class UberRequest extends Request {

    public UberRequest(Location location , String str) {
        criterias.put("Gps",new GpsCriteria(location));
        criterias.put("Keyword ", new KeyWordCriteria(str));
    }
}
