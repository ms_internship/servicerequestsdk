package mobilestudio.io.sdk.requestsdk.request;

/**
 * Created by pisoo on 10/5/2017.
 */

public class KeyWordCriteria extends Criteria<String> {
    public KeyWordCriteria(String value) {
        super(value);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value ;
    }
}
